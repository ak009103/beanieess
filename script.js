window.onload = function () {
  isUserLoggedIn();
} 

document.addEventListener('DOMContentLoaded', function () {
  var header = document.querySelector("header");

  window.addEventListener('scroll', function () {
    if(this.window.scrollY > 50){
      header.classList.add("shrink");
    } else {
      header.classList.remove("shrink");
    }
  });
});


let currentImageIndices = {
  'left-slider': 0,
  'center-slider': 0,
  'right-slider': 0
};

let intervals = {
  'left-slider': null,
  'center-slider': null,
  'right-slider': null
};

function rotateImages(sliderId) {
  const images = document.querySelectorAll('#' + sliderId + ' img');
  const captions = document.querySelectorAll('#' + sliderId + ' .caption');

  images.forEach(img => img.style.display = 'none');
  captions.forEach(caption => caption.style.display = 'none');

  images[currentImageIndices[sliderId]].style.display = 'block';
  captions[currentImageIndices[sliderId]].style.display = 'block';

  currentImageIndices[sliderId] = (currentImageIndices[sliderId] + 1) % images.length;
}

function startSlider(sliderId) {
  intervals[sliderId] = setInterval(() => rotateImages(sliderId), 3000);
}

function stopSlider(sliderId) {
  clearInterval(intervals[sliderId]);
}

startSlider('left-slider');
setTimeout(() => startSlider('center-slider'), 500);
setTimeout(() => startSlider('right-slider'), 1000);

document.querySelector('#left-slider').addEventListener('mouseenter', () => stopSlider('left-slider'));
document.querySelector('#left-slider').addEventListener('mouseleave', () => startSlider('left-slider'));

document.querySelector('#center-slider').addEventListener('mouseenter', () => stopSlider('center-slider'));
document.querySelector('#center-slider').addEventListener('mouseleave', () => startSlider('center-slider'));

document.querySelector('#right-slider').addEventListener('mouseenter', () => stopSlider('right-slider'));
document.querySelector('#right-slider').addEventListener('mouseleave', () => startSlider('right-slider'));




var loginModal = document.getElementById("loginModal");
var loginBtn = document.getElementById("loginBtn");
var loginSpan = document.querySelector(".login-close");
loginBtn.onclick = function() {
  loginModal.style.display = "block";
}
loginSpan.onclick = function() {
  loginModal.style.display = "none";
}
var signupModal = document.getElementById("signupModal");
var signupBtn = document.getElementById("signupBtn");
var signupSpan = document.querySelector(".signup-close");
signupBtn.onclick = function() {
  signupModal.style.display = "block";
}
// When clicks on x, close it
signupSpan.onclick = function() {
  signupModal.style.display = "none";
}
// When clicks outside close it
window.onclick = function(event) {
  if (event.target == loginModal) {
    loginModal.style.display = "none";
  }
  else if (event.target == signupModal) {
    signupModal.style.display = "none";
  }
}

var loginForm= document.getElementById("loginForm");
var signupForm= document.getElementById("signupForm");


loginForm.addEventListener("submit", function (event) {
  event.preventDefault();

  var username = document.getElementById("loginUsername").value;
  var password = document.getElementById("loginPassword").value;

  var storedUsername = localStorage.getItem(username);
  var storedPassword = localStorage.getItem(password);

  if (username === storedUsername && password === storedPassword) {
      alert("Login successful!");
      userLoggedIn();
      isUserLoggedIn();
  } else {
      alert("Incorrect username or password. Please try again.");
  }
});

signupForm.addEventListener("submit", function (event) {
  event.preventDefault();

  var username = document.getElementById("signupUsername").value;
  var password = document.getElementById("signupPassword").value;

  if (localStorage.getItem(username)) {
      alert("Username already exists. Please choose a different one.");
  } else {
      localStorage.setItem(username, password);
      alert("Signup successful! You can now log in.");
  }
});




// cat names
const catNames = ['Mittens', 'Shadow', 'Oliver', 'Bella', 'Charlie', 'Luna', 'Max', 'Lucy', 'Simba', 'Chloe'];

// to get a cat fact from API request
function fetchCatFact() {
  fetch('https://meowfacts.herokuapp.com/')
    .then(response => response.json())
    .then(data => {
      // Get the review section 
      const reviewSection = document.querySelector('.review-section');

      // Create a new div for the reviews if it doesn't exist
      let reviewsDiv = document.querySelector('.reviews');
      if (!reviewsDiv) {
        reviewsDiv = document.createElement('div');
        reviewsDiv.classList.add('reviews');
        reviewSection.appendChild(reviewsDiv);
      }

      // Clear the previous review
      reviewsDiv.innerHTML = '';

      const reviewDiv = document.createElement('div');
      reviewDiv.classList.add('review');

      // Create a new paragraph for the fact
      const factPara = document.createElement('p');
      factPara.textContent = data.data[0]; 

      // Create a new paragraph cat's name
      const reviewerPara = document.createElement('p');
      reviewerPara.classList.add('reviewer');
      reviewerPara.textContent = `- ${catNames[Math.floor(Math.random() * catNames.length)]}`;

      // Append the fact and reviewer
      reviewDiv.appendChild(factPara);
      reviewDiv.appendChild(reviewerPara);

      // Append the review
      reviewsDiv.appendChild(reviewDiv);
    })
    .catch(error => {
      console.error('Error:', error);
    });
}

fetchCatFact();

// get a new cat fact 
setInterval(fetchCatFact, 8000);




function isUserLoggedIn() {
  var isLoggedIn = localStorage.getItem("isLoggedIn");
  if (isLoggedIn === "true") {
      console.log("The user is logged in.");
      userLoggedIn();
  } else {
      console.log("The user is not logged in.");
      userLoggedOut();
  }
}

// Call this when the user logs in
function userLoggedIn() {
  localStorage.setItem("isLoggedIn", "true");

  // Hide buttons
  buttonGone("loginBtn");
  buttonGone("signupBtn");
  buttonShow("logoutBtn");
}

// Call this when the user logs out
function userLoggedOut() {
  
  localStorage.setItem("isLoggedIn", "false");

  // Show buttons
  buttonShow("loginBtn");
  buttonShow("signupBtn");
  buttonGone("logoutBtn");
}

function buttonGone(buttonId) {
  var button = document.getElementById(buttonId);
  button.style.display = "none";
}
function buttonShow(buttonId) {
  var button = document.getElementById(buttonId);
  button.style.display = "block"; 
}

var logoutBtn = document.getElementById("logoutBtn");

logoutBtn.onclick = function() {
    userLoggedOut();
    alert("You have been logged out.");
}