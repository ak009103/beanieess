
function getCartItems() {
    var cart = JSON.parse(localStorage.getItem('cart'));
  
    return cart;
  }
  
  function getTotalPrice() {
    var cart = JSON.parse(localStorage.getItem('cart'));
  
    var totalPrice = 0;
    for (var i = 0; i < cart.length; i++) {
        totalPrice += cart[i].price;
    }
  
    return totalPrice;
  }
  
  
  var productName = document.getElementById("productName").innerText;
  var productPrice = parseFloat(document.getElementById("productPrice").innerText.replace(/[^0-9.-]+/g,""));
  document.addEventListener("DOMContentLoaded", function() {
  let cart = JSON.parse(sessionStorage.getItem("cart")) || []; // Retrieve cart from session storage
  
    const modal = document.getElementById("cartModal");
    const addToCartBtn = document.getElementById("addToCartBtn");
    const viewCartBtn = document.getElementById("viewCartBtn");
    const continueShopping = document.getElementById("continueShopping");
  
    addToCartBtn.onclick = function() {
        const quantity = parseInt(document.getElementById("beanieQuantity").value);
        addToCart(quantity);
    }
  
    viewCartBtn.onclick = function() {
        updateCartModal();
    }
  
    closeBtn.onclick = function() {
        modal.style.display = "none";
    }
  
    continueShopping.onclick = function() {
        modal.style.display = "none";
    }
  
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
  
    function addToCart(quantity) {
        const product = { name: "Cozy Beanie", price: 19.99, quantity: quantity };
        if (cart.length === 0 || !cart.some(item => item.name === product.name)) {
            cart.push(product);
        } else {
            cart = cart.map(item => {
                if (item.name === product.name) {
                    item.quantity += quantity;
                }
                return item;
            });
        }
        updateCartModal();
        saveCartToStorage(); // Save cart to session storage
    }
  
    function updateCartModal() {
        let modalBody = document.getElementById("cartItems");
        modalBody.innerHTML = ""; // Clear modal body
        cart.forEach(item => {
            modalBody.innerHTML += `<p>${item.quantity} x ${item.name} - $${(item.price * item.quantity).toFixed(2)}</p>`;
        });
        modal.style.display = "block"; // Show the cart
    }
  
    function saveCartToStorage() {
        sessionStorage.setItem("cart", JSON.stringify(cart)); // Save cart to session storage
    }
  });
  


// Finns code
window.onload = function() {
    isUserLoggedIn();
}

var loginModal = document.getElementById("loginModal");
var loginBtn = document.getElementById("loginBtn");
var loginSpan = document.querySelector(".login-close");
loginBtn.onclick = function() {
  loginModal.style.display = "block";
}
loginSpan.onclick = function() {
  loginModal.style.display = "none";
}
var signupModal = document.getElementById("signupModal");
var signupBtn = document.getElementById("signupBtn");
var signupSpan = document.querySelector(".signup-close");
signupBtn.onclick = function() {
  signupModal.style.display = "block";
}
signupSpan.onclick = function() {
  signupModal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == loginModal) {
    loginModal.style.display = "none";
  }
  else if (event.target == signupModal) {
    signupModal.style.display = "none";
  }
}

var loginForm= document.getElementById("loginForm");
var signupForm= document.getElementById("signupForm");


loginForm.addEventListener("submit", function (event) {
  event.preventDefault();

  var username = document.getElementById("loginUsername").value;
  var password = document.getElementById("loginPassword").value;

  var storedUsername = localStorage.getItem(username);
  var storedPassword = localStorage.getItem(password);

  if (username === storedUsername && password === storedPassword) {
      alert("Login successful!");
      userLoggedIn();
      isUserLoggedIn();
  } else {
      alert("Incorrect username or password. Please try again.");
  }
});
  
  function isUserLoggedIn() {
    var isLoggedIn = localStorage.getItem("isLoggedIn");
    if (isLoggedIn === "true") {
        console.log("The user is logged in.");
        userLoggedIn();
    } else {
        console.log("The user is not logged in.");
        userLoggedOut();
    }
  }

  function userLoggedIn() {
    localStorage.setItem("isLoggedIn", "true");
    buttonGone("loginBtn");
    buttonGone("signupBtn");
    buttonShow("logoutBtn");
  }

  function userLoggedOut() {

    localStorage.setItem("isLoggedIn", "false");

    buttonShow("loginBtn");
    buttonShow("signupBtn");
    buttonGone("logoutBtn");
  }
  
  function buttonGone(buttonId) {
    var button = document.getElementById(buttonId);
    button.style.display = "none";
  }
  function buttonShow(buttonId) {
    var button = document.getElementById(buttonId);
    button.style.display = "block"; 
  }
  
  var logoutBtn = document.getElementById("logoutBtn");
  
  logoutBtn.onclick = function() {
      userLoggedOut();
      alert("You have been logged out.");
  }